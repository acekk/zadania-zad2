# -*- encoding: utf-8 -*-

import socket

import os.path

def print_file(path, extension):
    html = ''
    print(extension)

    ctype_jpg = "image/jpeg"
    ctype_png = "image/png"
    ctype_html = "text/html"
    ctype_plain = "text/plain"

    if extension == '.jpg':
        html_content_type = ctype_jpg
    if extension == '.png':
        html_content_type = ctype_png
    if extension == '.html':
        html_content_type = ctype_html
    if extension == '.txt':
        html_content_type = ctype_plain



    if extension in ('.txt', '.html', '.jpg', '.png'):
        html = "<html><body>"
        html += "<meta http-equiv='Content-Type' content='" + html_content_type + "; charset=utf-8'>"

        if extension in (".txt", ".html"):
            txt=open('web' + path).read()
            html+=txt

        if extension in (".jpg", ".png"):
            data_uri = open('web' + path, 'rb').read().encode('base64').replace('\n', '')

            html+= '<img src="data:image/png;base64,{0}"/>'.format(data_uri)
        html+="</body></html>"
    else:
        html = "<html><body>Not supported file extension</body></html>"
    return html

def list_directories(path):
    html_dir_list = '<html><body>'
    dir = ''
    splitted_path = str.split(path, "/")
    elem_list = os.listdir(path)
    if splitted_path.__len__() > 1:
        for dir_elem in splitted_path:
            if(dir_elem != 'web'):
                dir+='/'+dir_elem
    if elem_list.__len__() != 0:
        for elem in elem_list:
            html_dir_list+='<div><a href="' + dir + '/' + elem + '">' + elem + '</a></div>'
    else:
        html_dir_list += "<div>{empty}</div>"
    return html_dir_list + '</body></html>'

def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        request = '';
        try:
            # Odebranie żądania

            # while True:
            #     data = connection.recv(1024)
            #     request += data

            #     if  not data:
            #         print "No-data"
            #         break
            request = connection.recv(1024)

            if request:
                header_404 = "HTTP/1.1 404 ERROR"
                header_200 = "HTTP/1.1 200 OK"


                get_req_lines = str.split(request, "\r\n")
                get_uri_line = str.split(get_req_lines[0], " ");

                html_response = ''

                if get_uri_line[0].upper() == 'GET' and get_uri_line[2].upper() == "HTTP/1.1":
                    print "Request:"
                    print request
                    if  get_uri_line[1] != '/favicon.ico':
                        if get_uri_line[1] == '/':
                            html_response += header_200 + "\r\n\r\n" +  list_directories('./web')
                        else:
                            fileName, fileExtension = os.path.splitext(get_uri_line[1])
                            if fileExtension == '':
                                print get_uri_line[1]
                                html_response += header_200 + "\r\n\r\n" +  list_directories('web' + get_uri_line[1])
                            else:
                                print get_uri_line[1]
                                html_response += header_200 + "\r\n\r\n" + print_file(get_uri_line[1], fileExtension)

                # else:
                #     resp_header = "HTTP/1.1 401 ERROR"
                #     print resp_header
                #     response = resp_header + "\r\n" + "Wystąpił błąd"
                print(html_response)
                connection.sendall(html_response)
        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31012)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = open('web/web_page.html').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

