# -*- encoding: utf-8 -*-

import socket
from email.utils import formatdate
from datetime import datetime
from time import mktime

def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        request = '';
        try:
            # Odebranie żądania

            counter = 0

            while True:
                data = connection.recv(1024)
                request += data

                print data
                if  str(data) == "\r\n":
                    print "sieeemasz"
                    break



            if request:
                date = datetime.now()
                stamp = mktime(date.timetuple())
                date_now = formatdate(
                    timeval     = stamp,
                    localtime   = False,
                    usegmt      = True
                )

                print "Odebrano:"

                splitted_req = str.split(request, "\r\n")
                get_line = str.split(splitted_req[0], " ");
                if get_line[0].upper() == 'GET' and get_line[2].upper() == "HTTP/1.1":
                    print request
                    doc_header = "HTTP/1.1 200 OK" + "\r\n"+ "Content-Type: text/html; charset=UTF-8"+"\r\n"+"GMT: " + date_now

                    response = doc_header + "\r\n\r\n" + html + date_now;
                else:
                    resp_header = "HTTP/1.1 401 ERROR"
                    print resp_header
                    response = resp_header + "\r\n" + "Wystąpił błąd"

                connection.sendall(response)
        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 4444)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = open('web/web2.txt').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

